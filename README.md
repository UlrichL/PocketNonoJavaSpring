# PocketNonoJavaSpring

A Java app (servlet) based on Spring Boot. It displays a game board with interactive (clickable) fields.

![A screenshot of a web page with the title "Pocket Nono: Click some fields" and a number of grey or light red rectangles.](demo.png "Game board display")

## Current Features

* Controllers (Web/Html and basic post)
* Template rendering with Thymeleaf
* Simple frontend backend interaction with plain javascript (talking to the POST endpoint)
* Dependency injection (bean and auto-wiring)
* Usage of a database (Hibernate, JPA; H2) for saving and restoring the game state
* Unit tests

## Potential Improvements

* Add error handling
* Improve the game state (move logic out of the entity class)
* Support multiple users (session handling or additionally authentication)
* Add integration tests
* Extend "game play" (i. e. first define an actual target game state and check if it is reached)
