package de.ulrich.pocketnono;

import org.springframework.data.repository.CrudRepository;

/**
 * Database access wrapper for the game state.
 */
public interface GameStateRepository extends CrudRepository<GameState, Long> {
}