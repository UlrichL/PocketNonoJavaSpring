package de.ulrich.pocketnono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocketNonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocketNonoApplication.class, args);
	}
}
