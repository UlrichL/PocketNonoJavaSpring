package de.ulrich.pocketnono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Provide the current game state.
 */
@Service
public class GameService {
    private GameState state = new GameState();

    @Autowired
    private GameStateRepository repository;

    Logger logger = LoggerFactory.getLogger(GameService.class);

    public GameState getGameState() {
        Iterable<GameState> states = repository.findAll();

        for (GameState stateFromDb : states) {
            state = stateFromDb;

            logger.info("Got state from db with # fields active "+state.countActivatedFields());

            break; // There is at most one at the moment
        }

        return state;
    }
}
