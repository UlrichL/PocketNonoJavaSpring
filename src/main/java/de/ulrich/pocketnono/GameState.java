package de.ulrich.pocketnono;

import java.util.Arrays;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
@Access(AccessType.FIELD)
public class GameState {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    private int columnCount = 1;

    private Boolean[] fields = new Boolean[1];

    public GameState() {
        this(3, 6);
    }

    public GameState(int lines, int columns) {
        columnCount = columns;
        fields = new Boolean[lines*columnCount];
        Arrays.fill(fields, Boolean.FALSE);
    }

    public int getWidth() {
        return columnCount;
    }

    public int getHeight() {
        return fields.length / columnCount;
    }

    public void click(int line, int column) {
        fields[(line-1) * columnCount + (column-1)] = !fields[(line-1) * columnCount + (column-1)];
    }

    public int countActivatedFields() {
        return Arrays.stream(fields).mapToInt(field -> (field) ? 1 : 0).sum();
    }

    public boolean isActive(int line, int column) {
        return fields[(line-1) * columnCount + (column-1)];
    }
}
