package de.ulrich.pocketnono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller providing the main page of the application and a back channel.
 */
@Controller
public class GameController {
	@Autowired
    private GameService gameService;

	@Autowired
    private GameStateRepository repository;

    Logger logger = LoggerFactory.getLogger(GameController.class);
	
	@GetMapping("/")
    public String game(Model model) {
		model.addAttribute("game", gameService.getGameState());
        return "board";
    }
	
	@PostMapping("/click")
    public ResponseEntity<String> click(@RequestParam(defaultValue = "0") int x, @RequestParam(defaultValue = "0") int y) {
		if (x == 0 || y == 0) {
			return new ResponseEntity<>("missing x or y value", HttpStatus.BAD_REQUEST);
		}

		GameState gameState = gameService.getGameState();

		gameState.click(y, x);

		repository.save(gameState);

        return new ResponseEntity<>(""+gameState.countActivatedFields(), HttpStatus.OK);
    }
}
