package de.ulrich.pocketnono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class GameServiceTest {
    @MockBean
    private GameStateRepository gameStateRepositoryMock;

	@Autowired
	private GameService gameService;

	@Test
	void itDoesNotReturnAStateFromDbIfThereIsNone() {
		GameState state = gameService.getGameState();

		// Make sure this in _not_ the db test state
		assertNotEquals(10, state.getWidth());
		assertNotEquals(1, state.getHeight());
	}

	@Test
	void itReturnsTheStateFromDbIfThereIsOne() {
		GameState expectedGameState = new GameState(1, 10);
		Mockito.when(gameStateRepositoryMock.findAll()).thenReturn(List.of(expectedGameState));

		GameState state = gameService.getGameState();
		
		assertEquals(10, state.getWidth());
		assertEquals(1, state.getHeight());
	}
}
