package de.ulrich.pocketnono;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GameStateTest {
	@Test
	void itIsAnEmptyBoardInitially() {
		GameState state = new GameState(2, 2);

		assertEquals(2, state.getHeight());
		assertEquals(2, state.getWidth());
		assertEquals(0, state.countActivatedFields());

		for (int line=1; line <= 2; line++) {
			for (int column=1; column <= 2; column++) {
				assertEquals(false, state.isActive(line, column));
			}
		}
	}

	@Test
	void itCanActivateAField() {
		GameState state = new GameState(2, 2);

		assertEquals(false, state.isActive(1, 2));

		state.click(1, 2);

		assertEquals(true, state.isActive(1, 2));
		assertEquals(1, state.countActivatedFields());
	}
}
